## Based on example from Nikos but using autokeras for building the DNN

import tensorflow as tf
import numpy as np
import autokeras as ak

from numpy import loadtxt

from sklearn.preprocessing import StandardScaler

##########################
# Loading data           #
##########################

# Load the dataset
dataset = loadtxt('pima-indians-diabetes.data.csv', delimiter=',')

# Split into input (X) and output (y) variables
X = dataset[:,0:8]
y = dataset[:,8]


##########################
# Pre-processing of data #
##########################

print('Pre-processing...')

# Scaling
scaler = StandardScaler()
scaler.fit(X)
X_norm = scaler.transform(X)
dataset_norm = np.insert(X_norm,8, y, axis=1)

## Shuffling
np.random.shuffle(dataset_norm)

## Splitting into training and testing dataset
X = dataset_norm[:,0:8]
y = dataset_norm[:,8]

shape = dataset_norm.shape
N = shape[0]
N_train = int(2*N/3)

X_train = X[0:N_train,:]
y_train = y[0:N_train]

X_test = X[N_train:,:]
y_test = y[N_train:]

print("Will split test and training samples: train ", N_train,"total:", N)

##########################
# Feed into autokeras    #
##########################

## Initialize the structured data classifier.
clf = ak.StructuredDataClassifier(
    overwrite=True,
    max_trials=3) # It tries 3 different models.

# Feed the structured data classifier with training data.
clf.fit(X_train, y_train, epochs=10)

# Predict with the best model.
predicted_y = clf.predict(X_test)

# Evaluate the best model with testing data.
print(f"Best classifier: {clf.evaluate(X_test, y_test)}")

# Export as a Keras Model.
model = clf.export_model()

print(model.summary())

# Save the best model
try:
    model.save("AK_results/model_autokeras", save_format="tf")
except:
    model.save("AK_results/model_autokeras.h5")
